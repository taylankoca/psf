<?php namespace Tk\CarRental\Models;

use Model;

/**
 * Model
 */
class Vehicletype extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;
    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Attributes that support translation, if available.
     */
    public $translatable = ['name', 'data'];
    protected $dates = ['deleted_at'];
    protected $jsonable = ['data', 'extras'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_vehicletypes';

    public $belongsTo = [
        'category' => 'Tk\CarRental\Models\Category',
        'price' => 'Tk\CarRental\Models\Price',
    ];

    public $attachMany = [
        'images' => 'System\Models\File'
    ];

    public function beforeCreate() {
        if($this->data == []) {
            $this->data = [
                ["key" => "Klima", "value" => ""],
                ["key" => "USB", "value" => ""],
                ["key" => "ABS", "value" => ""],
                ["key" => "Hava Yastığı", "value" => ""],
                ["key" => "CD Çalar", "value" => ""],
                ["key" => "Motor Hacmi", "value" => ""],
                ["key" => "Motor Gücü", "value" => ""],

            ];
        }
    }

}
