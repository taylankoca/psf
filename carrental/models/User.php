<?php namespace Tk\CarRental\Models;

use Model;
use October\Rain\Support\Facades\Flash;
use Illuminate\Support\Facades\Hash;

/**
 * Model
 */
class User extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Hashable;
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $fillable = [
        'name', 'email', 'password', 'gender', 'birthday', 'phone', 'gsm', 'notes', 'guest'
    ];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_users';

    protected $hashable = ['password'];

}
