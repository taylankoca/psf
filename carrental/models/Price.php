<?php namespace Tk\CarRental\Models;

use Model;
use Carbon\Carbon;

/**
 * Model
 */
class Price extends Model
{

    use \October\Rain\Database\Traits\Revisionable;

    /**
     * @var array Monitor these attributes for changes.
     */
    protected $revisionable = ['name', 'email'];

    /**
     * @var array Relations
     */
    public $morphMany = [
        'revision_history' => ['System\Models\Revision', 'name' => 'revisionable']
    ];
    
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $jsonable = ['pricedata'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_prices';

    public function getDiff($start_time, $end_time, $reservation) {

        $start_time = Carbon::parse($start_time);
        $end_time = Carbon::parse($end_time);

        $diffInMinutes = $start_time->diffInMinutes($end_time);
        $diffInDays = $start_time->diffInDays($end_time);

        if(($diffInMinutes % 1440) > 180) {
            $diffInDays++;
        }

        $current_time = $start_time;

        $i = 0;
        do {
            $i++;
            $arr['daily'][] = $this->getInterval($current_time, $diffInDays, $reservation);
            $current_time->addDay();
        } while ($current_time->diffInMinutes($end_time) > 180 && $end_time->gt($current_time));

        $arr['diffInDays'] = $diffInDays;
        $arr['daysForPricing'] = $i;
        $arr['daysActual'] = $diffInDays;

        $arr['diffInMinutes'] = ($diffInMinutes % 1440);
        if( $arr['diffInMinutes'] > 180 ) {
            $arr['daysActual']--;
        }
        return $arr;
    }

    public function getInterval($time, $totalDaysForPricing, $reservation) {

        foreach($this->pricedata as $data) {
            if ($time->between(Carbon::parse($data["start_time"])->startOfDay(), Carbon::parse($data["end_time"])->startOfDay()) && $data['onlyforthiscity'] == 1 && $reservation->start_point->cityslug == $data['city']) {

                foreach ($data['prices'] as $price) {
                    $numOfDays = explode("-", $price["day_start_end"]);
                    if ($totalDaysForPricing >= intval($numOfDays[0]) && $totalDaysForPricing <= intval($numOfDays[1])) {
                        return $price["price"];
                    }
                }

            }
        }


        foreach($this->pricedata as $data) {
            if( $time->between(Carbon::parse($data["start_time"])->startOfDay(), Carbon::parse($data["end_time"])->startOfDay()) && $data['onlyforthiscity'] == 0) {

                foreach($data['prices'] as $price) {
                    $numOfDays = explode("-", $price["day_start_end"]);
                    if($totalDaysForPricing >= intval($numOfDays[0]) && $totalDaysForPricing <= intval($numOfDays[1])) {
                        return $price["price"];
                    }
                }

            }
        }

    }

    public function getDailyPriceArray($start_time, $end_time) {

        $arr = $this->diff($start_time, $end_time);

        $retarr = [];
        foreach($this->pricedata as $data) {

            //Eğer başlangıç aralıktaysa
            if( $arr['start_time']->between(Carbon::parse($data["start_time"])->startOfDay(), Carbon::parse($data["end_time"])->startOfDay()) ) {

                //Eğer bitiş de aralıktaysa
                if( $arr['end_time']->between(Carbon::parse($data["start_time"])->startOfDay(), Carbon::parse($data["end_time"])->startOfDay()) ) {

                    //O zaman gün sayısını bul

                    $diffInHours = $arr['start_time']->diffInHours($arr['end_time']);
                    $diffInMinutes = $arr['start_time']->diffInMinutes($arr['end_time']);
                    if(($diffInMinutes % 60) > 0) {

                        $diffInHours++;
                    }

                    $diffInDays = floor($diffInHours/24);

                    //3 saati geçerse 1 gün ekle
                    if($diffInHours % 24 > 3) {
                        $diffInDays += 1;
                    }

                    foreach($data['prices'] as $price) {
                        $numOfDays = explode("-", $price["day_start_end"]);
                        if($arr['daysForPricing'] >= intval($numOfDays[0]) && $arr['daysForPricing'] <= intval($numOfDays[1])) {
                            return [[ $diffInDays, $price["price"], ($diffInDays * $price["price"]) ]];
                        }
                    }

                    //eğer bitiş aynı aralıkta değilse dönem sonuna kadar olan gün sayısını bul yıkarı yuvarla
                } else {

                    $diffInHours = $arr['start_time']->diffInHours(Carbon::parse($data["end_time"])->startOfDay());
                    $diffInMinutes = $arr['start_time']->diffInMinutes(Carbon::parse($data["end_time"])->startOfDay());
                    if(($diffInMinutes % 60) > 0) {
                        $diffInHours++;
                    }

                    $diffInDays = ceil($diffInHours/24);


                    foreach($data['prices'] as $price) {
                        $numOfDays = explode("-", $price["day_start_end"]);
                        if($arr['daysForPricing'] >= intval($numOfDays[0]) && $arr['daysForPricing'] <= intval($numOfDays[1])) {
                            $retarr[] = [ $diffInDays, $price["price"], ($diffInDays * $price["price"]) ];
                        }
                    }

                }

            }

            //eğer bitiş aralıktaysa
            if( $arr['end_time']->between(Carbon::parse($data["start_time"])->startOfDay(), Carbon::parse($data["end_time"])->startOfDay()) ) {

                $diffInHoursAll = Carbon::parse($arr["start_time"])->diffInHours($arr['end_time']);
                $diffInMinutesAll = Carbon::parse($arr["start_time"])->diffInMinutes($arr['end_time']);

                $diffInHours = Carbon::parse($data["start_time"])->startOfDay()->diffInHours($arr['end_time']);
                $diffInMinutes = Carbon::parse($data["start_time"])->startOfDay()->diffInMinutes($arr['end_time']);

                if(($diffInMinutes % 60) > 0) {
                    $diffInHours++;
                }
                if(($diffInMinutesAll % 60) > 0) {
                    $diffInHoursAll++;
                }

                $diffInDays = floor($diffInHours/24);

                //3 saati geçerse 1 gün ekle
                if($diffInHoursAll % 24 <= 3 && $diffInHours % 24 <= 3) {
                    $diffInDays -= 1;
                }


                foreach($data['prices'] as $price) {
                    $numOfDays = explode("-", $price["day_start_end"]);
                    if($arr['daysForPricing'] >= intval($numOfDays[0]) && $arr['daysForPricing'] <= intval($numOfDays[1])) {
                        $retarr[] = [ $diffInDays, $price["price"], ($diffInDays * $price["price"]) ];
                    }
                }

            }



            //eğer başlangıç önce, bitiş de sonraysa bütün aralığı ekleyceksin
            if(
                $arr['start_time']->lt(Carbon::parse($data["start_time"])->startOfDay())
                &&
                $arr['end_time']->gte(Carbon::parse($data["end_time"])->startOfDay())
            ) {

                $diffInHours = Carbon::parse($data["start_time"])->startOfDay()->diffInHours(Carbon::parse($data["end_time"])->startOfDay());
                //Burası zaten tamsayı çıkmalı
                $diffInDays = ceil($diffInHours/24);

                foreach($data['prices'] as $price) {
                    $numOfDays = explode("-", $price["day_start_end"]);
                    if($arr['daysForPricing'] >= intval($numOfDays[0]) && $arr['daysForPricing'] <= intval($numOfDays[1])) {
                        $retarr[] = [ $diffInDays, $price["price"], ($diffInDays * $price["price"]) ];
                    }
                }

            }

        }

        return $retarr;

    }


    public function diff($res_start_time, $res_end_time) {

        $arr['start_time'] = Carbon::parse($res_start_time);
        $arr['end_time'] = Carbon::parse($res_end_time);

        $arr['diffInDays'] = $arr['start_time']->diffInDays($arr['end_time']);

        $arr['daysForPricing'] = 0;
        $arr['daysActual'] = 0;
        $arr['diffInHoursMod'] = ($arr['start_time']->diffInHours($arr['end_time']) % 24);

        if($arr['diffInHoursMod'] <= 3) {

            $arr['daysActual'] = $arr['start_time']->diffInDays($arr['end_time']);
            $arr['daysForPricing'] = $arr['daysActual'] + 0;

        } else {

            $arr['daysActual'] = $arr['start_time']->diffInDays($arr['end_time']);
            $arr['daysForPricing'] = $arr['daysActual'] + 1;

        }

        return $arr;

    }




    public function beforeCreate() {

        if($this->pricedata == []) {
            $this->pricedata = [
                ["start_time" => "2018-05-01 04:00:00", "end_time" => "2018-07-01 04:00:00", "onlyforthiscity" => 0, "prices" => ["1" => ["day_start_end" => "1-3", "price" => ""], "2" => ["day_start_end" => "4-7", "price" => ""], "3" => ["day_start_end" => "8-15", "price" => ""], "4" => ["day_start_end" => "16-21", "price" => ""], "5" => ["day_start_end" => "22-28", "price" => ""], "6" => ["day_start_end" => "29-99", "price" => ""], ], ],
                ["start_time" => "2018-07-01 04:00:00", "end_time" => "2018-08-27 04:00:00", "onlyforthiscity" => 0, "prices" => ["1" => ["day_start_end" => "1-3", "price" => ""], "2" => ["day_start_end" => "4-7", "price" => ""], "3" => ["day_start_end" => "8-15", "price" => ""], "4" => ["day_start_end" => "16-21", "price" => ""], "5" => ["day_start_end" => "22-28", "price" => ""], "6" => ["day_start_end" => "29-99", "price" => ""], ], ],
                ["start_time" => "2018-08-27 04:00:00", "end_time" => "2018-11-01 04:00:00", "onlyforthiscity" => 0, "prices" => ["1" => ["day_start_end" => "1-3", "price" => ""], "2" => ["day_start_end" => "4-7", "price" => ""], "3" => ["day_start_end" => "8-15", "price" => ""], "4" => ["day_start_end" => "16-21", "price" => ""], "5" => ["day_start_end" => "22-28", "price" => ""], "6" => ["day_start_end" => "29-99", "price" => ""], ], ],
                ["start_time" => "2018-11-01 04:00:00", "end_time" => "2019-05-01 04:00:00", "onlyforthiscity" => 0, "prices" => ["1" => ["day_start_end" => "1-3", "price" => ""], "2" => ["day_start_end" => "4-7", "price" => ""], "3" => ["day_start_end" => "8-15", "price" => ""], "4" => ["day_start_end" => "16-21", "price" => ""], "5" => ["day_start_end" => "22-28", "price" => ""], "6" => ["day_start_end" => "29-99", "price" => ""], ], ],
            ];
        }
    }



}
