<?php namespace Tk\CarRental\Models;

use Model;
use Tk\CarRental\Models\Setting;

/**
 * Model
 */
class Interval extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_intervals';

    public $belongsTo = [
        'point_1' => 'Tk\CarRental\Models\Point',
        'point_2' => 'Tk\CarRental\Models\Point',
    ];

    public function getDiffInPlacePrice ($res_start_point_id, $res_end_point_id, $res_currency) {

        $setting = Setting::find(1);
        $arr['diffInPlacePrice'] = 0;
        $arr['diffInPlace'] = false;

        if($res_start_point_id != $res_end_point_id) {

            $arr['diffInPlace'] = Interval::where("point_1_id", "=", $res_start_point_id)->where("point_2_id", "=", $res_end_point_id)->first();

            $arr['diffInPlacePrice'] = $arr['diffInPlace']->price;

            if($arr['diffInPlace']) {

                if($res_currency == "EUR") {

                    $arr['diffInPlacePrice'] = $arr['diffInPlace']->price;

                } else if ($res_currency == "USD") {

                    $arr['diffInPlacePrice'] = $arr['diffInPlace']->price * $setting['eurtousd'];

                } else if ($res_currency == "TRY") {

                    $arr['diffInPlacePrice'] = $arr['diffInPlace']->price * $setting['eurtotry'];

                }

            } else {

                $arr['diffInPlace'] = Interval::where("point_2_id", "=", $res_start_point_id)->where("point_1_id", "=", $res_end_point_id)->first();

                if($arr['diffInPlace']) {

                    if($res_currency == "EUR") {

                        $arr['diffInPlacePrice'] = $arr['diffInPlace']->price;

                    } else if ($res_currency == "USD") {

                        $arr['diffInPlacePrice'] = $arr['diffInPlace']->price * $setting['eurtousd'];

                    } else if ($res_currency == "TRY") {

                        $arr['diffInPlacePrice'] = $arr['diffInPlace']->price * $setting['eurtotry'];

                    }

                } else {

                    $arr['diffInPlace'] = false;
                    $arr['diffInPlacePrice'] = 0;

                }

            }
        }

        return $arr;

    }

}
