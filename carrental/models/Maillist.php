<?php namespace Tk\CarRental\Models;

use Model;

/**
 * Model
 */
class Maillist extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];
    protected $fillable = ['name', 'email', 'gsm'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_maillist';
}
