<?php namespace Tk\CarRental\Models;

use Model;

/**
 * Model
 */
class Setting extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;
    /**
     * Softly implement the TranslatableModel behavior.
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var array Attributes that support translation, if available.
     */
    public $translatable = ['news', 'extras', 'contactinfo', 'terms'];

    protected $dates = ['deleted_at'];
    protected $jsonable = ['settings', 'showcase', 'news', 'extras', 'contactinfo'];
    protected $fillable = ['buyeur', 'selleur', 'buyusd', 'sellusd'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_settings';


    public $belongsToMany = [
        'vehicletype' => ['Tk\CarRental\Models\Vehicletype', 'table' => 'tk_carrental_setting_vehicletype', 'key' => 'vehicletype_id', 'otherKey' => 'setting_id'],
    ];

    public function eurtousd ($unit = 1.0) {
        return $unit * $this->eurtousd;
    }

    public function eurtotry ($unit = 1.0) {
        return $unit * $this->eurtotry;
    }


}
