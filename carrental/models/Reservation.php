<?php namespace Tk\CarRental\Models;

use Model;
use Illuminate\Support\Str;
use Backend\Facades\BackendAuth;
use Carbon\Carbon;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;
use Renatio\DynamicPDF\Classes\PDF;

use Tk\CarRental\Models\Setting;

/**
 * Model
 */
class Reservation extends Model
{
    use \October\Rain\Database\Traits\Revisionable;

    /**
     * @var array Monitor these attributes for changes.
     */
    protected $revisionable = [
        'user_id', 'vehicletype_id', 'start_time',
        'end_time', 'start_point_id', 'end_point_id',
        'final_price', 'status', 'start_subpoint_id',
        'end_subpoint_id', 'extras', 'vehicle_id',
        'pnr', 'currency', 'notes', 'payment_type',
        'arrival_flight_no', 'arrival_flight_info',
        'return_flight_no', 'return_flight_info', 'usernotes'];


    /**
     * @var array Relations
     */
    public $morphMany = [
        'revision_history' => ['System\Models\Revision', 'name' => 'revisionable']
    ];

    public function getRevisionableUser()
    {
        if(isset(BackendAuth::getUser()->id)) {
            return BackendAuth::getUser()->id;
        } else {
            return 0;
        }
    }

    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];


    protected $fillable = [
        'user_id', 'vehicletype_id', 'start_time',
        'end_time', 'start_point_id', 'end_point_id',
        'final_price', 'status', 'start_subpoint_id',
        'end_subpoint_id', 'extras', 'vehicle_id',
        'pnr', 'currency', 'notes', 'payment_type',
        'arrival_flight_no', 'arrival_flight_info',
        'return_flight_no', 'return_flight_info'];


    protected $jsonable = ['extras'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_reservations';

    public $belongsToMany = [
        'usernote' => ['Tk\CarRental\Models\Usernote', 'table' => 'tk_carrental_reservation_usernote']
    ];

    public $belongsTo = [
        'user' => 'Tk\CarRental\Models\User',
        'vehicletype' => 'Tk\CarRental\Models\Vehicletype',
        'vehicle' => 'Tk\CarRental\Models\Vehicle',
        'start_point' => 'Tk\CarRental\Models\Point',
        'end_point' => 'Tk\CarRental\Models\Point',
        'start_subpoint' => 'Tk\CarRental\Models\Subpoint',
        'end_subpoint' => 'Tk\CarRental\Models\Subpoint',
    ];


    public function beforeCreate () {
        $this->pnr = strtoupper(Str::random(6));
    }

    public function beforeSave() {
        $setting = Setting::find(1);
        $reservation = Reservation::with('start_point', 'start_subpoint', 'end_point', 'end_subpoint', 'user', 'vehicletype')->find($this->id);


        if($this->decided_price != "" && $this->decided_price != $reservation->decided_price) {
            if($this->currency == "EUR") {
                $this->secret_eur_price_for_total = $this->decided_price;
            } else if ($this->currency == "USD") {
                $this->secret_eur_price_for_total = $this->decided_price / $setting->eurtousd;
            } else if ($this->currency == "TRY") {
                $this->secret_eur_price_for_total = $this->decided_price / $setting->eurtotry;
            }
        } else {
            if($this->currency == "EUR") {
                $this->secret_eur_price_for_total = $this->final_price;
            } else if ($this->currency == "USD") {
                $this->secret_eur_price_for_total = $this->final_price / $setting->eurtousd;
            } else if ($this->currency == "TRY") {
                $this->secret_eur_price_for_total = $this->final_price / $setting->eurtotry;
            }
        }

        $start_time = Carbon::parse($this->start_time);
        $end_time = Carbon::parse($this->end_time);

        $diffInMinutes = $start_time->diffInMinutes($end_time);
        $diffInDays = $start_time->diffInDays($end_time);

        if(($diffInMinutes % 1440) > 180) {
            $diffInDays++;
        }

        $this->total_days = $diffInDays;


        if(isset($this->status)) {
            if($reservation->status != $this->status) {

                if($this->status == "ready") {

                    Mail::send('reservation.ready', ['reservation' => $reservation->toArray()], function($message) use ($reservation) {
                        $message->to($reservation->user->email);
                        $message->attach($reservation->exportPDF());
                    });

                } else if($this->status == "approved") {

                    Mail::send('reservation.approve', ['reservation' => $reservation->toArray()], function($message) use ($reservation) {
                        $message->to($reservation->user->email);
                        $message->attach($reservation->exportPDF());
                    });

                } else if ($this->status == "cancelledbyuser") {

                    Mail::send('reservation.cancelledbyuser', ['reservation' => $reservation->toArray()], function($message) use ($reservation) {
                        $message->to($reservation->user->email);
                        $message->attach($reservation->exportPDF());
                    });

                } else if ($this->status == "cancelledbypsf") {

                    Mail::send('reservation.cancelledbypsf', ['reservation' => $reservation->toArray()], function($message) use ($reservation) {
                        $message->to($reservation->user->email);
                        $message->attach($reservation->exportPDF());
                    });

                }
            }
        }


    }

    public function exportPDF() {
        $setting = Setting::find(1);
        $templateCode = 'psf::reservation_user'; // unique code of the template
        $dir = storage_path("app/media/pdf/").$this->pnr."-pasifik.pdf";
        PDF::loadTemplate($templateCode, ["reservation" => $this, "terms" => $setting->terms])->save($dir);
        return "/var/www/html/storage/app/media/pdf/" . $this->pnr . "-pasifik.pdf";
    }


}
