<?php namespace Tk\CarRental\Models;

use Model;

/**
 * Model
 */
class Vehicle extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_vehicles';

    public $belongsTo = [
        'vehicletype' => 'Tk\CarRental\Models\Vehicletype',
    ];

    public $hasMany = [
        'reservation' => 'Tk\CarRental\Models\Reservation',
    ];

    public function scopeActive($query , $model)
    {
        return $query->where('is_active', 1);
    }
}
