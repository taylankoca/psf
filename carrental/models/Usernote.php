<?php namespace Tk\CarRental\Models;

use Model;
use Backend\Facades\BackendAuth;

/**
 * Model
 */
class Usernote extends Model
{
    use \October\Rain\Database\Traits\Revisionable;

    /**
     * @var array Monitor these attributes for changes.
     */
    protected $revisionable = [
        'note', 'username'];


    /**
     * @var array Relations
     */
    public $morphMany = [
        'revision_history' => ['System\Models\Revision', 'name' => 'revisionable']
    ];
    public function getRevisionableUser()
    {
        return BackendAuth::getUser()->id;
    }
    use \October\Rain\Database\Traits\Validation;
    
    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'tk_carrental_usernotes';

    public $belongsTo = [
        'reservation' => 'Tk\CarRental\Models\Reservation',
    ];


    public function beforeCreate () {
        $this->username = BackendAuth::getUser()->first_name." ".BackendAuth::getUser()->last_name;
        $this->user_id = BackendAuth::getUser()->id;
    }

    public function beforeUpdate() {
        if($this->user_id == BackendAuth::getUser()->id) {
            \Flash::success("Kendi notunuzu güncellediniz.");
        } else {
            \Flash::error("Başkasının notunu güncelleyemezsiniz.");
            unset($this->note);
        }
    }
    public function beforeDelete() {
        if($this->user_id == BackendAuth::getUser()->id) {
            \Flash::success("Kendi notunuzu sildiniz.");
        } else {
            \Flash::error("Başkasının notunu silemezsiniz.");
            unset($this->id);
        }
    }

}
