<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalVehicletypes6 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->text('seo_keywords')->nullable();
            $table->text('seo_description')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->dropColumn('seo_keywords');
            $table->dropColumn('seo_description');
        });
    }
}
