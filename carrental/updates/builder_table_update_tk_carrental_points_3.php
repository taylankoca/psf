<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalPoints3 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->dropColumn('sort_order');
        });
    }
}
