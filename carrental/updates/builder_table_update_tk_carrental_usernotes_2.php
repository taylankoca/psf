<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsernotes2 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_usernotes', function($table)
        {
            $table->integer('user_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_usernotes', function($table)
        {
            $table->dropColumn('user_id');
        });
    }
}
