<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsers5 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->string('password', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->string('password', 191)->nullable(false)->change();
        });
    }
}
