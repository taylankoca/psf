<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalPrices extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_prices', function($table)
        {
            $table->text('name')->nullable();
            $table->dropColumn('vehicletype_id');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_prices', function($table)
        {
            $table->dropColumn('name');
            $table->integer('vehicletype_id');
        });
    }
}
