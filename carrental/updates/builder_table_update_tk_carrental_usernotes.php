<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsernotes extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_usernotes', function($table)
        {
            $table->string('username')->nullable();
            $table->dropColumn('reservation_id');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_usernotes', function($table)
        {
            $table->dropColumn('username');
            $table->integer('reservation_id');
        });
    }
}
