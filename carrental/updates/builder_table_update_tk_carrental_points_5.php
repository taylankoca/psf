<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalPoints5 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->string('cityslug')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->dropColumn('cityslug');
        });
    }
}
