<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTkCarrentalSettingVehicletype extends Migration
{
    public function up()
    {
        Schema::create('tk_carrental_setting_vehicletype', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('setting_id')->nullable();
            $table->integer('vehicletype_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tk_carrental_setting_vehicletype');
    }
}
