<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalVehicletypes5 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->integer('price_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->dropColumn('price_id');
        });
    }
}
