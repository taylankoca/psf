<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTkCarrentalIntervals extends Migration
{
    public function up()
    {
        Schema::create('tk_carrental_intervals', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('point_1');
            $table->integer('point_2');
            $table->decimal('price', 10, 4);
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tk_carrental_intervals');
    }
}
