<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalCategories extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_categories', function($table)
        {
            $table->string('slug');
            $table->integer('sort_order')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_categories', function($table)
        {
            $table->dropColumn('slug');
            $table->integer('sort_order')->nullable(false)->change();
        });
    }
}
