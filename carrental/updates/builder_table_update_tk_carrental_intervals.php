<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalIntervals extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_intervals', function($table)
        {
            $table->integer('point_1_id');
            $table->integer('point_2_id');
            $table->dropColumn('point_1');
            $table->dropColumn('point_2');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_intervals', function($table)
        {
            $table->dropColumn('point_1_id');
            $table->dropColumn('point_2_id');
            $table->integer('point_1');
            $table->integer('point_2');
        });
    }
}
