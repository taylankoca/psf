<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalPoints4 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->boolean('is_active')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->dropColumn('is_active');
        });
    }
}
