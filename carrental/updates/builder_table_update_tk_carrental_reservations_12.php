<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations12 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->integer('total_days')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->dropColumn('total_days');
        });
    }
}
