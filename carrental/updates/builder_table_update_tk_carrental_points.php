<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalPoints extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->integer('subpoint_id')->nullable();
            $table->integer('sort_order')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_points', function($table)
        {
            $table->dropColumn('subpoint_id');
            $table->dropColumn('sort_order');
        });
    }
}
