<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations11 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->decimal('secret_eur_price_for_total', 10, 2)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->dropColumn('secret_eur_price_for_total');
        });
    }
}
