<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsers4 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->text('notes')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->text('notes')->nullable(false)->change();
        });
    }
}
