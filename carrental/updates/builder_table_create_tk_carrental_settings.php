<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTkCarrentalSettings extends Migration
{
    public function up()
    {
        Schema::create('tk_carrental_settings', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('settings');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tk_carrental_settings');
    }
}
