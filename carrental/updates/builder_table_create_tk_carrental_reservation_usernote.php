<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTkCarrentalReservationUsernote extends Migration
{
    public function up()
    {
        Schema::create('tk_carrental_reservation_usernote', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('reservation_id');
            $table->integer('usernote_id');
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tk_carrental_reservation_usernote');
    }
}
