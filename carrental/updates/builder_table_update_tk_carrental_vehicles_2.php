<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalVehicles2 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_vehicles', function($table)
        {
            $table->integer('vehicletype_id');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_vehicles', function($table)
        {
            $table->dropColumn('vehicletype_id');
        });
    }
}
