<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsers extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->string('password_repeat')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->dropColumn('password_repeat');
        });
    }
}
