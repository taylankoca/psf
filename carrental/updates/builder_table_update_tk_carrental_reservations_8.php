<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations8 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->text('arrival_flight_no')->nullable();
            $table->text('arrival_flight_info')->nullable();
            $table->text('return_flight_no')->nullable();
            $table->text('return_flight_info')->nullable();
            $table->string('payment_type')->nullable();
            $table->text('notes')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->dropColumn('arrival_flight_no');
            $table->dropColumn('arrival_flight_info');
            $table->dropColumn('return_flight_no');
            $table->dropColumn('return_flight_info');
            $table->dropColumn('payment_type');
            $table->dropColumn('notes');
        });
    }
}
