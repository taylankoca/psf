<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations6 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->integer('user_id')->nullable()->change();
            $table->integer('vehicletype_id')->nullable()->change();
            $table->dateTime('start_time')->nullable()->change();
            $table->dateTime('end_time')->nullable()->change();
            $table->integer('start_point_id')->nullable()->change();
            $table->integer('end_point_id')->nullable()->change();
            $table->string('status', 191)->nullable()->change();
            $table->integer('start_subpoint_id')->nullable()->change();
            $table->integer('end_subpoint_id')->nullable()->change();
            $table->integer('vehicle_id')->nullable()->change();
            $table->string('pnr', 191)->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->integer('user_id')->nullable(false)->change();
            $table->integer('vehicletype_id')->nullable(false)->change();
            $table->dateTime('start_time')->nullable(false)->change();
            $table->dateTime('end_time')->nullable(false)->change();
            $table->integer('start_point_id')->nullable(false)->change();
            $table->integer('end_point_id')->nullable(false)->change();
            $table->string('status', 191)->nullable(false)->change();
            $table->integer('start_subpoint_id')->nullable(false)->change();
            $table->integer('end_subpoint_id')->nullable(false)->change();
            $table->integer('vehicle_id')->nullable(false)->change();
            $table->string('pnr', 191)->nullable(false)->change();
        });
    }
}
