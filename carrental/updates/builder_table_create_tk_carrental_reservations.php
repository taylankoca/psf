<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateTkCarrentalReservations extends Migration
{
    public function up()
    {
        Schema::create('tk_carrental_reservations', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('vehicletype_id');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->integer('start_point_id');
            $table->integer('end_point_id');
            $table->decimal('final_price', 10, 2)->nullable();
            $table->string('status');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('tk_carrental_reservations');
    }
}
