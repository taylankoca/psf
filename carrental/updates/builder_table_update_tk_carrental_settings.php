<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalSettings extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_settings', function($table)
        {
            $table->decimal('buyeur', 10, 4)->nullable();
            $table->decimal('selleur', 10, 4)->nullable();
            $table->decimal('buyusd', 10, 4)->nullable();
            $table->decimal('sellusd', 10, 4)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_settings', function($table)
        {
            $table->dropColumn('buyeur');
            $table->dropColumn('selleur');
            $table->dropColumn('buyusd');
            $table->dropColumn('sellusd');
        });
    }
}
