<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalSubpoints2 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_subpoints', function($table)
        {
            $table->boolean('is_active')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_subpoints', function($table)
        {
            $table->dropColumn('is_active');
        });
    }
}
