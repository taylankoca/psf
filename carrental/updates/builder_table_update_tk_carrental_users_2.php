<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalUsers2 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->text('notes');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_users', function($table)
        {
            $table->dropColumn('notes');
        });
    }
}
