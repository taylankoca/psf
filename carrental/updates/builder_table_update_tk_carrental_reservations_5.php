<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations5 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->string('currency')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->dropColumn('currency');
        });
    }
}
