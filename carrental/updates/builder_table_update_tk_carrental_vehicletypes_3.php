<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalVehicletypes3 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->text('extras')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_vehicletypes', function($table)
        {
            $table->dropColumn('extras');
        });
    }
}
