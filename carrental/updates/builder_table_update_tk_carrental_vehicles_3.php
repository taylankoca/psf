<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalVehicles3 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_vehicles', function($table)
        {
            $table->boolean('is_active')->nullable();
            $table->integer('vehicletype_id')->nullable()->change();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_vehicles', function($table)
        {
            $table->dropColumn('is_active');
            $table->integer('vehicletype_id')->nullable(false)->change();
        });
    }
}
