<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalSettings2 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_settings', function($table)
        {
            $table->text('showcase')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_settings', function($table)
        {
            $table->dropColumn('showcase');
        });
    }
}
