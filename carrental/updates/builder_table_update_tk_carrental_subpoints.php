<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalSubpoints extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_subpoints', function($table)
        {
            $table->integer('sort_order')->nullable();
            $table->integer('point_id')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_subpoints', function($table)
        {
            $table->dropColumn('sort_order');
            $table->dropColumn('point_id');
        });
    }
}
