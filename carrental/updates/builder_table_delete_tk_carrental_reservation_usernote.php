<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableDeleteTkCarrentalReservationUsernote extends Migration
{
    public function up()
    {
        Schema::dropIfExists('tk_carrental_reservation_usernote');
    }
    
    public function down()
    {
        Schema::create('tk_carrental_reservation_usernote', function($table)
        {
            $table->engine = 'InnoDB';
            $table->integer('id');
            $table->integer('reservation_id');
            $table->integer('usernote_id');
        });
    }
}
