<?php namespace Tk\CarRental\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateTkCarrentalReservations3 extends Migration
{
    public function up()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->integer('vehicle_id');
        });
    }
    
    public function down()
    {
        Schema::table('tk_carrental_reservations', function($table)
        {
            $table->dropColumn('vehicle_id');
        });
    }
}
