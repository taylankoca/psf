<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use Backend;
use BackendMenu;
use Illuminate\Support\Facades\Input;
use Tk\CarRental\Models\Reservation;
use Tk\CarRental\Models\Setting;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Illuminate\Support\Facades\Response;
use Renatio\DynamicPDF\Classes\PDF;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Mail;


use RainLab\Translate\Classes\Translator;

use Tecnickcom\TCPDF;

class Reservations extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'manage_carrental' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'carrental', 'reservations');
    }

    public function onApprove () {
        $checked = Input::get('checked');
        foreach($checked as $checked_id) {
            $reservation = Reservation::with('start_point', 'start_subpoint', 'end_point', 'end_subpoint', 'user', 'vehicletype')->find($checked_id);
            if($reservation->status == "ready") {
                $reservation->status = "approved";
                $reservation->save();
            }


        }
        \Flash::success("İşlem başarıyla gerçekleştirildi.");
    }




    public function onExcelExport () {
        $checked = Input::get('checked');

        $spreadsheet = new Spreadsheet();
        $i = 1;
        $total_decided_price = 0;
        $total_of_total_days = 0;
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            //print_r( $reservation->toArray() );

            $sheet = $spreadsheet->getActiveSheet();

            if($i == 1) {
                $sheet->setCellValue('A'.$i, "PNR");

                $sheet->setCellValue('B'.$i, "ADI");
                $sheet->setCellValue('C'.$i, "Email");
                $sheet->setCellValue('D'.$i, "Telefon");

                $sheet->setCellValue('E'.$i, "Araç Tipi");
                $sheet->setCellValue('F'.$i, "Araç Özellikleri");

                    $sheet->setCellValue('G'.$i, "Plaka");

                $sheet->setCellValue('H'.$i, "Alış Noktası");
                $sheet->setCellValue('I'.$i, "Alış Saati");

                $sheet->setCellValue('J'.$i, "Dönüş Noktası");
                $sheet->setCellValue('K'.$i, "Dönüş Saati");

                $sheet->setCellValue('L'.$i, "Gün Sayısı");

                $sheet->setCellValue('M'.$i, "Son Fiyat");
                $sheet->setCellValue('N'.$i, "Kararlaştırılan Fiyat");

                $sheet->setCellValue('O'.$i, "Ödeme Tipi");

                $sheet->setCellValue('P'.$i, "Varış Uçuş No");
                $sheet->setCellValue('Q'.$i, "Varış Uçuş Bilgisi");
                $sheet->setCellValue('R'.$i, "Dönüş Uçuş No");
                $sheet->setCellValue('S'.$i, "Dönüş Uçuş Bilgisi");

                $sheet->setCellValue('T'.$i, "Ekstralar");
                $sheet->setCellValue('U'.$i, "Notlar");
                $sheet->setCellValue('V'.$i, "Durum");

                $i++;
            }

            $sheet->setCellValue('A'.$i, $reservation->pnr);

            $sheet->setCellValue('B'.$i, $reservation->user->name);
            $sheet->setCellValue('C'.$i, $reservation->user->email);
            $sheet->setCellValue('D'.$i, $reservation->user->phone);

            $sheet->setCellValue('E'.$i, $reservation->vehicletype->name);
            $sheet->setCellValue('F'.$i, $reservation->vehicletype->fuel." ".$reservation->vehicletype->gearbox);
            if(isset($reservation->vehicle->name)) {
                $sheet->setCellValue('G'.$i, $reservation->vehicle->name);
            }

            $sheet->setCellValue('H'.$i, $reservation->start_point->name." ".$reservation->start_subpoint->name);
            $sheet->setCellValue('I'.$i, $reservation->start_time);

            $sheet->setCellValue('J'.$i, $reservation->end_point->name." ".$reservation->end_subpoint->name);
            $sheet->setCellValue('K'.$i, $reservation->end_time);

            $sheet->setCellValue('L'.$i, $reservation->total_days);
            
            $total_of_total_days += $reservation->total_days;

            $sheet->setCellValue('M'.$i, $reservation->final_price." ".$reservation->currency);
            $sheet->setCellValue('N'.$i, $reservation->decided_price." ".$reservation->currency);

            $sheet->setCellValue('O'.$i, $reservation->payment_type);

            $sheet->setCellValue('P'.$i, $reservation->arrival_flight_no);
            $sheet->setCellValue('Q'.$i, $reservation->arrival_flight_info);
            $sheet->setCellValue('R'.$i, $reservation->departure_flight_no);
            $sheet->setCellValue('S'.$i, $reservation->departure_flight_info);

            $extra_text = "";
            if(!empty($reservation->extras)) {
                foreach($reservation->extras as $extra) {

                    //print_r($extra);
                    $extra_text .= $extra['key']." ".$extra['total']." ".$reservation->currency."\n";
                }
            }

            $sheet->setCellValue('T'.$i, $extra_text);

            $sheet->setCellValue('U'.$i, $reservation->notes);

            $sheet->setCellValue('V'.$i, $reservation->status);

            $sheet->setCellValue('W'.$i, $reservation->secret_eur_price_for_total);

            $total_decided_price += $reservation->secret_eur_price_for_total;

            $i++;
        }

        $sheet->setCellValue('K'.$i, "Toplam gün sayısı:");
        $sheet->setCellValue('L'.$i, $total_of_total_days);

        $sheet->setCellValue('V'.$i, "Toplam kayıtlı fiyat:");
        $sheet->setCellValue('W'.$i, $total_decided_price." EUR");
        $writer = new Xlsx($spreadsheet);
        $path = 'rezervasyonlar.xlsx';

        $writer->save(temp_path($path));

        return Backend::redirect('tk/carrental/reservations/downloadExcelFile');


    }

    public function downloadExcelFile() {
        // Here we can make use of the download response
        return Response::download(temp_path('rezervasyonlar.xlsx'));
    }


    public function onPDFExport () {

        $this->translator = Translator::instance();
        $this->translator->setLocale("tr");

        $checked = Input::get('checked');

        $pnr = Input::get("pnr");
        $reservation = Reservation::
        with('start_point', 'start_subpoint', 'end_point', 'end_subpoint', 'user', 'vehicletype', 'vehicle')
            ->where("id", "=", $checked[0])
            ->first();

        $setting = Setting::find(1);
        $templateCode = 'psf::reservation_comp'; // unique code of the template
        $dir = storage_path("app/media/pdf/").$reservation->pnr."-comp-pasifik.pdf";
        PDF::loadTemplate($templateCode, ["reservation" => $reservation, "terms" => $setting->terms])->save($dir);
        return Redirect::to( "storage/app/media/pdf/" . $reservation->pnr . "-comp-pasifik.pdf" );
    }

    protected $translator;


    public function onPDFExportEN () {

        $this->translator = Translator::instance();
        $this->translator->setLocale("en");

        $checked = Input::get('checked');

        $pnr = Input::get("pnr");
        $reservation = Reservation::
        with('start_point', 'start_subpoint', 'end_point', 'end_subpoint', 'user', 'vehicletype', 'vehicle')
            ->where("id", "=", $checked[0])
            ->first();

        $setting = Setting::find(1);
        $templateCode = 'psf::reservation_comp'; // unique code of the template
        $dir = storage_path("app/media/pdf/").$reservation->pnr."-comp-pasifik.pdf";
        PDF::loadTemplate($templateCode, ["reservation" => $reservation, "terms" => $setting->lang("en")->terms])->save($dir);
        return Redirect::to( "storage/app/media/pdf/" . $reservation->pnr . "-comp-pasifik.pdf" );
    }


    public function onPDFExportDE () {

        $this->translator = Translator::instance();
        $this->translator->setLocale("de");

        $checked = Input::get('checked');

        $pnr = Input::get("pnr");
        $reservation = Reservation::
        with('start_point', 'start_subpoint', 'end_point', 'end_subpoint', 'user', 'vehicletype', 'vehicle')
            ->where("id", "=", $checked[0])
            ->first();

        $setting = Setting::find(1);
        $templateCode = 'psf::reservation_comp'; // unique code of the template
        $dir = storage_path("app/media/pdf/").$reservation->pnr."-comp-pasifik.pdf";
        PDF::loadTemplate($templateCode, ["reservation" => $reservation, "terms" => $setting->lang("de")->terms])->save($dir);
        return Redirect::to( "storage/app/media/pdf/" . $reservation->pnr . "-comp-pasifik.pdf" );
    }



    public function onTotalEUR () {
        $setting = Setting::find(1);
        $checked = Input::get('checked');
        $total = 0.0;
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            $total += $reservation->secret_eur_price_for_total;
        }
        \Flash::success("Toplam: ".number_format($total, 2)." €");
        return true;
    }



}
