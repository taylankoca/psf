<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Users extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'manage_carrental' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'carrental', 'users');
    }

    public function beforeCreate () {
        if($this->password != "" && $this->password == $this->password_repeat) {
            unset($this->password_repeat);
        } else {
            Flash::error("Şifreleri kontrol edin.");
        }
    }
}
