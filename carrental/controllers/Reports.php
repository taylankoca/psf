<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Reports extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController'    ];
    
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = [
        'manage_carrental' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'settings', 'reports');
    }
}
