<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use BackendMenu;
use Carbon\Carbon;

class Vehicles extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController'
    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'manage_carrental' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'carrental', 'vehicles');
    }

}
