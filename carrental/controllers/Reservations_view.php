<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use Backend;
use BackendMenu;
use Illuminate\Support\Facades\Input;
use Tk\CarRental\Models\Reservation;
use Tk\CarRental\Models\Setting;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use Illuminate\Support\Facades\Response;

class Reservations_view extends Controller
{
    public $implement = [
        'Backend\Behaviors\ListController',
        'Backend\Behaviors\FormController',
        'Backend\Behaviors\RelationController',
    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $relationConfig = 'config_relation.yaml';

    public $requiredPermissions = [
        'view_carrental'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'reservations_view', 'reservations_view');
    }

    public function onApprove () {
        $checked = Input::get('checked');
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            if($reservation->status == "ready") {
                $reservation->status = "approved";
                $reservation->save();
            }
        }
        \Flash::success("İşlem başarıyla gerçekleştirildi.");
    }




    public function onExcelExport () {
        $checked = Input::get('checked');
        $spreadsheet = new Spreadsheet();
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            //print_r( $reservation->toArray() );

            $sheet = $spreadsheet->getActiveSheet();
            $sheet->setCellValue('A1', 'Hello World !');


        }
        $writer = new Xlsx($spreadsheet);
        $path = 'rezervasyonlar.xlsx';

        $writer->save(temp_path($path));

        return Backend::redirect('tk/carrental/reservations/downloadExcelFile');
    }

    public function downloadExcelFile() {
        // Here we can make use of the download response
        return Response::download(temp_path('rezervasyonlar.xlsx'));
    }


    public function onPDFExport () {
        $checked = Input::get('checked');
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            if($reservation->status == "ready") {
                $reservation->status = "approved";
                //$reservation->save();
            }
        }
        \Flash::success("İşlem başarıyla gerçekleştirildi.");
    }

    public function onPDFExportOne () {
        \Flash::success(time());
    }

    public function onTotalEUR () {
        $setting = Setting::find(1);
        $checked = Input::get('checked');
        $total = 0.0;
        foreach($checked as $checked_id) {
            $reservation = Reservation::find($checked_id);
            $total += $reservation->secret_eur_price_for_total;
        }
        \Flash::success("Toplam: ".number_format($total, 2)." €");
        return true;
    }


}
