<?php namespace Tk\CarRental\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Points extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController',        'Backend\Behaviors\ReorderController'    ];
    
    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public $requiredPermissions = [
        'manage_carrental' 
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Tk.CarRental', 'settings', 'points');
    }
}
