<?php namespace Tk\CarRental;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function registerComponents()
    {
    }

    public function registerSettings()
    {
    }

    public function registerSchedule($schedule)
    {
        $schedule->call(function () {

            \Db::table('tk_carrental_reservations')
                ->where("start_time", "<", Date("Y-m-d H:i:s", (time() - (21 * 60 * 60))) )
                ->where("status", "=", "approved")
                ->update(["status" => "completed"]);


        })->everyMinute();


        $schedule->call(function () {

            $connect_web = simplexml_load_file('http://www.tcmb.gov.tr/kurlar/today.xml');

            $eurtousd = $connect_web->Currency[3]->CrossRateOther;
            $eurtotry = $connect_web->Currency[3]->BanknoteSelling;

            \Db::table('tk_carrental_settings')
                ->where("id", "=", 1)
                ->update(
                    ["eurtousd" => $eurtousd,
                    "eurtotry" => $eurtotry]
                );

        })->everyMinute();
    }
}
